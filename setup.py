from setuptools import setup

setup(name='argon2_easy_secure',
        use_scm_version=True,
        description='An easy way to use the most secure variant of Argon2: Argon2id.',
        url='https://gitlab.com/demilletech/python-argon2-secure',
        author='demilleTech LLC',
        license='MIT',
        py_modules=['argon2_easy_secure'],
        setup_requires=['setuptools_scm'],
        install_requires=['argon2_cffi']
        classifiers=[
            'Development Status :: 4 - Beta',
            'License :: OSI Approved :: MIT License',
            'Topic :: Security :: Cryptography'
        ])
