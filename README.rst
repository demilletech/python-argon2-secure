argon2_easy_secure
=============
A simple rewrite of the :code:`argon2_cffi` default PasswordHasher, defaulting to Argon2id instead of Argon2i, and allowing for future users to change the hash type.
To get started: ::
        pip install argon2_easy_secure

        >>> # Replace this:
        >>> import argon2
        >>> # With this:
        >>> import argon2_easy_secure as argon2
